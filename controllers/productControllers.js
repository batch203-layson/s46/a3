const Product = require("../models/Product");
const auth = require("../auth");

module.exports.createProduct = (req, res) => {
	const token = req.headers.authorization;
	const user = auth.decode(token);
	//console.log(user);
	if (user.isAdmin) {
		Product.findOne({ name: req.body.name })
			.then(product => {
				if (product != null && product.name == req.body.name) {
					return res.send(false);
				}
				else {
					let newProduct = new Product({
						name: req.body.name,
						description: req.body.description,
						price: req.body.price,
						stocks: req.body.stocks
					});
					newProduct.save()
						.then(product => {
							//console.log(product);
							return res.send(product);
						})
						.catch(error => {
							//console.log(error);
							return res.send(error);
						});
				}
			})
			.catch(error => {
				//console.log(error);
				return res.send(error);
			});
	} else {
		return res.send(false);
	}
};

module.exports.viewActiveProducts = (req, res) => {
	Product.find({isActive: true})
	.then(products => {
		return res.send(products);
	})
	.catch(error => {
		//console.log(error);
		return res.send(error);
	})
};

module.exports.viewAllProducts = (req, res) => {

	const userData = auth.decode(req.headers.authorization);

	if (userData.isAdmin) {
		return Product.find({}).then(result => res.send(result));
	}
	else {
		// return res.send(false);
		return res.status(401).send("You don't have access to this page!");
	}

}

module.exports.viewProduct = (req, res) => {
	Product.findById(req.params.productId)
	.then(product => {
		return res.send(product);
	})
	.catch(error => {
		//console.log(error);
		return res.send(error);
	})
};

// Update product admin only
module.exports.updateProduct = (req, res) => {
	const token = req.headers.authorization;
	const user = auth.decode(token);

	if (user.isAdmin) {
		let updateProduct = {
			name: req.body.name,
			description: req.body.description,
			price: req.body.price,
			stocks: req.body.stocks
		}
		return Product.findByIdAndUpdate(req.params.productId, updateProduct, { new: true })
			.then(result => {
				res.send(result);
			})
			.catch(error => {
				//console.log(error);
				res.send(error);
			});
	}
	else {
		return res.send(false);
	}
}


module.exports.archiveProduct = (req, res) => {
	const token = req.headers.authorization;
	const user = auth.decode(token);

	if(user.isAdmin){
		let archiveProduct = {
			isActive: req.body.isActive
		};
		return Product.findByIdAndUpdate(req.params.productId, archiveProduct, {new: true})
		.then(archivedProduct => {
			return res.send(archivedProduct);
		})
		.catch(error => {
			//console.log(error);
			return res.send(error);
		});
	}
	else{
		return res.send("You don't have access to this page");
	}
};
