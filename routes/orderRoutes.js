const express = require("express");
const router = express.Router();
const orderControllers = require("../controllers/orderControllers");
const auth = require("../auth");

router.post("/", auth.verify, orderControllers.order);
router.get("/myOrders", auth.verify, orderControllers.retrieveOrders);
router.get("/allOrders", auth.verify, orderControllers.retrieveAllOrders);

module.exports = router;
