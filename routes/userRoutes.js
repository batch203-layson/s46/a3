const express = require("express");
const router = express.Router();
const userController = require("../controllers/userControllers");
const auth = require("../auth");
//User
router.post("/register", userController.createUser);
router.post("/login", userController.login);
router.get("/viewDetails", auth.verify, userController.viewUserDetails);
router.patch("/setadmin/:userId", auth.verify, userController.appointAdmin);

module.exports = router;
